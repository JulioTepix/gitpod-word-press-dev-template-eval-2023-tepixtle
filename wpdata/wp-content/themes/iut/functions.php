<?php

function iut_scripts() {
    $parenthandle = 'twentynineteen-style';
    $theme        = wp_get_theme();

    wp_enqueue_style( 'twentynineteen-print-style', get_template_directory_uri() . '/print.css', array(), wp_get_theme()->get( 'Version' ), 'print' );

    wp_enqueue_style(
        $parenthandle,
        get_template_directory_uri() . '/style.css',
        array(),
        $theme->parent()->get( 'Version' )
    );
    
    wp_enqueue_style(
        'iut-style',
        get_stylesheet_uri(),
        array( $parenthandle ),
        $theme->get( 'Version' )
    );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'iut_scripts' );


function iut_register_post_type_recette() {

	register_post_type(
		'recette',
		array(
			'labels'				=> array(
				'name'					=> 'Recettes',
				'singular_name'			=> 'Recette',
			),
			'public'				=> true,    
			'publicly_queryable'	=> true,   
			'show_in_rest'			=> true,	
			'hierarchical'			=> false,
			'supports'				=> array( 'title', 'editor', 'thumbnail' ),
			'has_archive'			=> 'recettes',
			'rewrite'				=> array( 'slug' => 'recipe' ),
		)
	);

}

add_action( 'init', 'iut_register_post_type_recette', 10);

function iut_add_meta_boxes_recette( $post ) {

	add_meta_box(
		'iut_mbox_recette',                
		'Infos complémentaires',  
		'iut_mbox_recettes_content',
		'recettes'                          	
	);

}

add_action( 'add_meta_boxes', 'iut_add_meta_boxes_recette' );

function iut_mbox_recettes_content( $post ) {

	// Get meta value
	$recette_ingredients = get_post_meta(
		$post->ID,
		'recette-ingredients',
		true
	);

	echo '<p>';
	echo '<label for="recette-ingredients">';
	echo 'Ingredients: ';
	echo '<input type="textarea" id="recette-ingredients" name="recette-ingredients" value="' . $recette_ingredients . '">';
	echo '</label>';
	echo '</p>';
}

function iut_save_post( $post_id ) {

	if ( isset( $_POST['recette-ingredients'] ) && !empty( $_POST['recette-ingredients'] ) ) {

		update_post_meta(
			$post_id,
			'recette-ingredients',
			sanitize_text_field( $_POST['recette-ingredients'] )
		);

	}

}
add_action( 'save_post', 'iut_save_post' );

function iut_register_ctx_recette_type() {

	register_taxonomy(
		'recette-saison',
		'recette',
		array(
			'labels'				=> array(
				'name'					=> 'Saison de recette',
				'singular_name'			=> 'Saison'
			),
			'public'				=> true,    // false = cachée de l'interface d'admin et du frontend
			'publicly_queryable'	=> true,    // Visible côté frontend ?
			'hierarchical'			=> true,   	// Fonctionne comme les catégories (true) ou comme les étiquettes (false)
			'show_in_rest'			=> true,	// Nécessaire pour fonctionner avec Gutenberg
			'rewrite'				=> array( 'slug' => 'Saison de recette' ),
		)
	);

}

add_action( 'init', 'iut_register_ctx_recette_type', 11 );



